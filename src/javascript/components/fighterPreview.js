import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if(fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterName = createElement({ tagName: 'h4' });
    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-details'
    });
    const fighterDetailsWrapper = createElement({
      tagName: 'div',
      className: 'fighter-details__wrapper'
    });
    const svgIcons = {
        attack: `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="48" height="48"
                    viewBox="0 0 172 172"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#000000"><path d="M148.77132,14.42432c-0.77196,-0.0277 -1.56534,0.05365 -2.35856,0.25195l-26.97998,6.74674c-3.35545,0.8402 -6.36344,2.7312 -8.5734,5.396l-59.04101,67.56543l-6.80973,-6.80973c-1.34815,-1.40412 -3.35005,-1.96971 -5.23364,-1.47866c-1.88359,0.49105 -3.35456,1.96202 -3.84561,3.84561c-0.49105,1.88359 0.07455,3.88549 1.47866,5.23364l8.74137,8.74137l-22.86475,22.86474c-2.17444,-1.90288 -5.46417,-1.74639 -7.44816,0.35431c-1.98399,2.1007 -1.9524,5.39399 0.07153,7.45624l21.5,21.5c2.06225,2.02393 5.35555,2.05553 7.45624,0.07154c2.1007,-1.98399 2.25719,-5.27373 0.3543,-7.44817l22.86474,-22.86474l8.74137,8.74137c1.34815,1.40412 3.35005,1.96971 5.23364,1.47866c1.88359,-0.49105 3.35456,-1.96202 3.84561,-3.84561c0.49105,-1.88359 -0.07455,-3.88549 -1.47866,-5.23364l-6.80274,-6.80273l67.68441,-59.04102c2.69369,-2.22778 4.59761,-5.26343 5.43099,-8.65039c0,-0.00233 0,-0.00467 0,-0.007l6.59977,-26.93799c1.35911,-5.55659 -3.16268,-10.93402 -8.56641,-11.12793zM146.90267,22.99072v0.014l-0.007,-0.007zM146.2098,25.8042l-5.9139,24.13151c-0.28534,1.15428 -0.93107,2.18019 -1.84066,2.93245c-0.03538,0.02987 -0.07038,0.06021 -0.10498,0.09098l-68.34928,59.60791l-10.56803,-10.56803l59.61491,-68.20931c0.03078,-0.0346 0.06111,-0.0696 0.09098,-0.10498c0.74987,-0.9042 1.76641,-1.54869 2.90446,-1.83366zM53.75,111.51725l6.73275,6.73275l-22.85775,22.85775l-6.73275,-6.73275z"></path></g></g></svg>`,
        defense: `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="48" height="48"
                    viewBox="0 0 172 172"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#000000"><path d="M86.007,17.93766c-3.74842,0 -7.49921,1.15763 -10.66601,3.47135c-8.72706,6.37615 -24.7428,15.95804 -42.4541,17.80469c-6.55118,0.68452 -11.65095,6.42574 -11.36589,13.08757c2.91373,68.18195 46.3565,94.47214 59.111,100.5433c3.38028,1.6064 7.36273,1.6064 10.743,0c12.7545,-6.07116 56.19728,-32.36135 59.111,-100.5433c0,-0.00233 0,-0.00467 0,-0.007c0.27875,-6.65935 -4.81608,-12.39555 -11.36589,-13.08057h-0.007c-17.70839,-1.84825 -33.72118,-11.42937 -42.44711,-17.80469c-3.1668,-2.31372 -6.91059,-3.47135 -10.65902,-3.47135zM86.007,28.64567c1.50295,0 3.00226,0.48028 4.3182,1.44173c9.57661,6.99685 26.89427,17.65362 47.67513,19.82031c0.00233,0 0.00467,0 0.007,0c1.09371,0.11377 1.77931,0.89615 1.73568,1.93864c-2.72877,63.78306 -42.28509,86.19197 -52.98014,91.28402c-0.52556,0.24976 -0.99316,0.24976 -1.51872,0c-10.69564,-5.09224 -50.25415,-27.50221 -52.98014,-91.29101c-0.0445,-1.04001 0.63617,-1.81737 1.73568,-1.93164c0.00233,0 0.00467,0 0.007,0c20.78087,-2.16669 38.09852,-12.82346 47.67513,-19.82031c1.31594,-0.96145 2.82224,-1.44173 4.32519,-1.44173z"></path></g></g></svg>`,
        health: `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="48" height="48"
                    viewBox="0 0 172 172"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#000000"><path d="M53.75,25.08333c-21.70625,0 -39.41667,17.71042 -39.41667,39.41667c0,14.67789 11.06406,28.49789 24.55143,42.67106c13.48738,14.17317 30.08859,28.52788 43.31494,41.75423c2.0991,2.09823 5.50149,2.09823 7.60059,0c13.22635,-13.22635 29.82756,-27.58106 43.31494,-41.75423c13.48737,-14.17317 24.55143,-27.99318 24.55143,-42.67106c0,-21.70625 -17.71042,-39.41667 -39.41667,-39.41667c-12.45723,0 -23.30306,6.68825 -32.25,18.04964c-8.94694,-11.36139 -19.79277,-18.04964 -32.25,-18.04964zM53.75,35.83333c10.49405,0 19.22924,5.58426 27.70785,18.99447c0.98543,1.5569 2.6996,2.50061 4.54215,2.50061c1.84255,0 3.55672,-0.94371 4.54215,-2.50061c8.4786,-13.4102 17.2138,-18.99447 27.70785,-18.99447c15.89725,0 28.66667,12.76942 28.66667,28.66667c0,8.61378 -8.64428,21.66878 -21.58399,35.26644c-11.79982,12.39981 -26.4674,25.35443 -39.33268,37.89795c-12.86528,-12.54352 -27.53286,-25.49814 -39.33268,-37.89795c-12.93971,-13.59766 -21.58399,-26.65266 -21.58399,-35.26644c0,-15.89725 12.76942,-28.66667 28.66667,-28.66667z"></path></g></g></svg>`
    };
    fighterDetails.innerHTML = `
      <div class="fighter-detail__cell">
        <p>
          ${svgIcons.attack}
          ${fighter.attack}
        </p>
      </div>
      <div class="fighter-detail__cell">
        <p>
          ${svgIcons.defense}
          ${fighter.defense}
        </p>
      </div>
      <div class="fighter-detail__cell">
        <p>
          ${svgIcons.health}
          ${fighter.health}
        </p>
      </div>
    `;
    fighterName.innerText = fighter.name;
    fighterDetailsWrapper.append(fighterName, fighterDetails);
    fighterElement.append(fighterImg, fighterDetailsWrapper);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
